"""
Initialization file for the package __loggers__.
"""
# from src.packages.loaders import config
from .logger import *

__all__ = ["Log", "logger", "Loggers"]

# todo: ImportError: cannot import name 'Loggers' from partially initialized module 'src.packages.logger' (most likely due to a circular import)
# logger = Log(config["log_filename"], config["log_encoding"], config["logging_formats"], config["logging_date_format"])
logger = Log(
    "amfi-bot.log",
    "utf-8",
    {"app": "%(asctime)s - %(name)s - %(levelname)s - %(message)s", "incoming": "%(asctime)s - %(name)s - %(message)s"},
    "%Y-%m-%d %H:%M",
)
