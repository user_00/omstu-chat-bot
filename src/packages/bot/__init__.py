"""
Initialization file for the package __bot__.
"""
from .bot_telegram import *
from .bot_vk import *
from .bot_ds import *
