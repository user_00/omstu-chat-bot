# OmSTU chat-bot

## What is it?

This is a simple chat bot for a telegram, which will try to answer the questions of applicants and help with admission to the department of AMaFI at OmSTU.

## Getting Started

_Run all commands from the root of the project_

### Preparing

#### Install python 3.10.x

##### Windows:

[download python 3.10](https://www.python.org/downloads/release/python-3100/).

##### Linux:

```shell
$ sudo apt-get install python3.10
```

#### Install dependencies

##### Windows:

Initialization and launch of the virtual environment with poetry.

```shell
> pip install poetry

> poetry env use %PATH_TO_YOUR_PYTHON_3.10%
```

Install _Microsoft C++ Build Tools_ 14.0 or greater.

Install dependencies.

```shell
> poetry install --only main
```

##### Linux:

Initialization and launch of the virtual environment with poetry.

```shell
$ curl -sSL https://install.python-poetry.org | python3 -

$ echo "export PATH=$PATH:~/.local/share/pypoetry/venv/bin" >> ~/.bashrc

$ bash

$ poetry env use python3.10
```

Install dependencies.

```shell
$ sudo apt-get install python3.10-dev

$ poetry install --only main
```

#### Environment variables

Rename the .env.dist file to .env and fill in the indicated fields.

### Launch

```shell
 poetry run python -m src.main
```

### Stopping

```shell
 ctrl + c
```

### Dev. env.

#### Preparation

##### Pre-commit

To enable pre-commit on the current repository:

```shell
 poetry run pre-commit install
```

#### Commands

- `poetry run black src/` – launch style checker.
- `poetry run pylint src/` – launch linter.
- `poetry run python -m src.main` – launch app.

## Authors

* **Lonskii Denis** – [GitLab](https://gitlab.com/denisLonskii)
* **Nelin Maxim** – [GitLab](https://gitlab.com/user_00)

## License

This project is licensed under the MIT License.