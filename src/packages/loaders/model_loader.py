from __future__ import annotations
import pathlib

# pylint: disable=unused-import
from os import PathLike
import pickle
from typing import TypeAlias
import joblib
import dill
from keras import models

__all__ = ["ModelLoader"]


class ModelLoaderException(Exception):
    """
    Exception class for the model loader.
    """


PathType: TypeAlias = "pathlib.Path | PathLike | str"


class ModelLoader:
    __path = pathlib.Path("")
    __encoding = "utf-8"
    __reading_mode = "rb"

    @classmethod
    def __set_loader(cls, loader: str):
        """
        :param loader: the name of the loader that will load the models
        :raise ModelLoaderException: if the specified loader is missing
        :raise ModelLoaderException: if failed to determine loader
        """
        loader = loader.lower()
        if loader == "pickle":
            cls.__loader_func = cls.__loader_for_pickle
        elif loader == "joblib":
            cls.__loader_func = cls.__loader_for_joblib
        elif loader == "dill":
            cls.__loader_func = cls.__loader_for_dill
        elif loader == "keras":
            cls.__loader_func = cls.__loader_for_keras
        elif loader == "auto":
            file_suffix = cls.__path.suffix
            if file_suffix in {".pkl", ".pickle"}:
                cls.__loader_func = cls.__loader_for_pickle
            elif file_suffix in {".joblib"}:
                cls.__loader_func = cls.__loader_for_joblib
            elif file_suffix in {".dill"}:
                cls.__loader_func = cls.__loader_for_dill
            elif file_suffix in {".h5"}:
                cls.__loader_func = cls.__loader_for_keras
            else:
                raise ModelLoaderException(f"Failed to determine loader for file: {loader}.")
        else:
            raise ModelLoaderException(f"Invalid parameter: {loader}.")

    @classmethod
    def __set_path(cls, path: PathType):
        """
        :param path: path to file
        :raise ModelLoaderException: if unexpected error, failed to generate path
        :raise ModelLoaderException: if file not found
        :raise ModelLoaderException: if the path does not point to the file
        """

        try:
            cls.__path = pathlib.Path(path).resolve()
            if not cls.__path.exists():
                raise ModelLoaderException(f"File not found, check your path: {path}.")
            if not cls.__path.is_file():
                raise ModelLoaderException(f"Specify the path to the file, check your path: {path}.")
        except Exception as exception:
            raise ModelLoaderException(f"Unexpected error, failed to generate path: {exception}") from exception

    @classmethod
    def load(cls, path: PathType, loader: str = "auto", reading_mode: str = "rb", encoding: str = "utf-8") -> any:
        """
        The method loads the file with model.
        :param path: path to the file with the model.
        :param loader: model loading library.  Pickle allows you to load
            most data types in python, joblib is big data oriented,
            and dill allows you to load anonymous functions.
            If set to `auto`, then the method will try to determine
            the loader itself based on the file extension
        :param reading_mode: the mode of reading the file with the model.
        :param encoding: file encoding with model (used in reading mode `r`).
        :raise ModelLoaderException: if unexpected error.
        """

        cls.__reading_mode = reading_mode
        cls.__encoding = encoding
        cls.__set_path(path)
        cls.__set_loader(loader)
        try:
            model = cls.__loader_func()
        except Exception as exception:
            raise ModelLoaderException(f"Unexpected error: {exception}") from exception
        return model

    @classmethod
    def __loader_for_pickle(cls) -> any:
        """
        The method loads the model with path and read mode by using library __pickle__.
        :return: loaded model.
        """

        if cls.__reading_mode == "rb":
            with open(cls.__path, cls.__reading_mode) as file:
                model = pickle.load(file)
        else:
            with open(cls.__path, cls.__reading_mode, encoding=cls.__encoding) as file:
                model = pickle.load(file)
        return model

    @classmethod
    def __loader_for_joblib(cls) -> any:
        """
        The method loads the model with path and read mode by using library __joblib__.
        :return: loaded model.
        """

        if cls.__reading_mode == "rb":
            with open(cls.__path, cls.__reading_mode) as file:
                model = joblib.load(file)
        else:
            with open(cls.__path, cls.__reading_mode, encoding=cls.__encoding) as file:
                model = joblib.load(file)
        return model

    @classmethod
    def __loader_for_dill(cls) -> any:
        """
        The method loads the model with path and read mode by using library __dill__.
        :return: loaded model.
        """

        if cls.__reading_mode == "rb":
            with open(cls.__path, cls.__reading_mode) as file:
                model = dill.load(file)
        else:
            with open(cls.__path, cls.__reading_mode, encoding=cls.__encoding) as file:
                model = dill.load(file)
        return model

    @classmethod
    def __loader_for_keras(cls) -> any:
        """
        The method loads the `Sequential` model with path by using library __keras__.
        :return: loaded `Sequential` model.
        """
        return models.load_model(cls.__path)
