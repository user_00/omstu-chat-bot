#!/bin/sh

SESSION_NAME="chat_bot"
tmux has-session -t $SESSION_NAME

if [ $? != 0 ]
 then
 tmux new-session -s $SESSION_NAME -d
 tmux send-keys -t $SESSION_NAME "cd /home/omstu-chat-bot" C-m
 tmux send-keys -t $SESSION_NAME "/root/.local/share/pypoetry/venv/bin/poetry run python -m src.main" C-m
fi