# pylint: skip-file
from aiogram import Bot
from aiogram.types.message import Message
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from .IBot import IBot
from src.packages.logger import logger, Loggers

__all__ = ["BotTelegram"]


import asyncio


class BotTelegram(IBot):
    _IBot__chat_bot = None
    _IBot__logger = None
    __bot = None
    __dp = None

    def __init__(self, api_key: str, platform="tg", chat_model=None):
        super().__init__(api_key, platform, chat_model)

    def start(self):
        logger.info(Loggers.APP.value, "Starting TG bot...")
        loop = asyncio.new_event_loop()  # создаем новый цикл событий
        asyncio.set_event_loop(loop)  # устанавливаем его в качестве текущего цикла событий
        loop.create_task(executor.start_polling(self.__dp))  # запускаем выполнение задачи в цикле событий
        loop.run_forever()  # запускаем бесконечный цикл событий

    def _IBot__authorization(self, api_key: str):
        self.__bot = Bot(api_key)
        self.__dp = Dispatcher(self.__bot)
        self.__register_handlers()

    async def _IBot__handle_start(self, message: Message):
        """
        This handler will be called when user sends `/start` command.
        """
        await self.__bot.send_message(message.from_user.id, self._IBot__get_start_message())

    async def _IBot__handle_help(self, message: Message):
        """
        This handler will be called when user sends `/help` command.
        """
        await self.__bot.send_message(message.from_user.id, self._IBot__get_start_message())

    async def _IBot__handle_text(self, message: Message):
        """
        This handler will be called when the user sends any text.
        Based on the received question, and answer is generated and sent to the user.
        """
        try:
            self._IBot__chat_bot.set_user_id(message.from_user.id)
            question = message.text
            answer = self._IBot__chat_bot.generate_answer(question)
            await self.__bot.send_message(message.from_user.id, answer)
        except RuntimeError as error:
            self._IBot__logger.error("app", str(error))

    def __register_handlers(self):
        self.__dp.register_message_handler(self._IBot__handle_start, commands=["start", "about"])
        self.__dp.register_message_handler(self._IBot__handle_help, commands=["help"])
        self.__dp.register_message_handler(self._IBot__handle_text, content_types=["text"])
