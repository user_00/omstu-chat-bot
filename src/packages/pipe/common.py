__all__ = [
    "PipelineException",
    "ReshapeTransformer",
    "ModelPrediction",
    "NormalizationStep",
]

from typing import Any

import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin


class PipelineException(Exception):
    """
    Class-exception for any errors in pipeline steps.
    """


class ReshapeTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, shape):
        self.shape = shape

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X.reshape(self.shape)


class ModelPrediction(BaseEstimator, TransformerMixin):
    def __init__(self, model: Any = None):
        if model is None:
            raise PipelineException(f"Invalid parameter: {model}.")
        self.model = model

    def fit(self, X, y=None):
        self.model.fit(X, y)
        return self

    def predict(self, X, y=None):
        return self.model.predict(X, verbose=0)[0]

    def transform(self, X, y=None):
        return self.predict(X)


class NormalizationStep(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    @staticmethod
    def predict_func(X, y=None):
        intention_probability_vector = X.sum(axis=1)
        intention_probability_vector = intention_probability_vector.reshape(-1, 1)
        intention_probability_vector = np.divide(intention_probability_vector, sum(intention_probability_vector))
        return intention_probability_vector

    def transform(self, X, y=None):
        return self.predict_func(X)

    def predict(self, X):
        return self.transform(X)
