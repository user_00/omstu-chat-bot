from pathlib import Path
import numpy as np
from scipy.sparse import csr_matrix
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.preprocessing import OneHotEncoder
import tensorflow as tf
import torch
from transformers import AutoTokenizer, AutoModel
from transformers.modeling_outputs import BaseModelOutputWithPoolingAndCrossAttentions
from src.packages.path_storage import PathStorage

__all__ = [
    "ToOneHotVectorIntents",
    "ToMultiOneHotVectorPatterns",
    "ToBagOfWordsVectorPatterns",
    "ToTFIDFVectorPatterns",
    "SparseTensorTransformer",
    "ToSentenceEmbeddingVectorPatternsSBert",
]


class ToOneHotVectorIntents(OneHotEncoder):
    def __init__(self, sparse_output=False, handle_unknown="error", **kwargs):
        super().__init__(sparse_output=sparse_output, handle_unknown=handle_unknown, **kwargs)

    def fit(self, X: np.ndarray[np.str_], y: None = None):
        """
        :param X: array of all intentions
        :param y: Ignored. This parameter exists only for compatibility with :class:`~sklearn.pipeline.Pipeline`.
        :return: self
        """
        super().fit(X.reshape(-1, 1), y)
        return self

    def transform(self, X: str) -> np.ndarray[np.int_]:
        """
        :param X: definite intention
        :return: one-hot vector definite intention
        """
        return super().transform(np.array(X).reshape(-1, 1)).reshape(-1)


class ToMultiOneHotVectorPatterns(OneHotEncoder):
    def __init__(self, sparse_output=False, handle_unknown="ignore", **kwargs):
        super().__init__(sparse_output=sparse_output, handle_unknown=handle_unknown, **kwargs)

    def fit(self, X: np.ndarray[np.str_], y: None = None):
        """
        :param X: array of all words
        :param y: Ignored. This parameter exists only for compatibility with :class:`~sklearn.pipeline.Pipeline`.
        :return: self
        """
        super().fit(X.reshape(-1, 1), y)
        return self

    def transform(self, X: list[str]) -> np.ndarray[np.int_]:
        """
        :param X: array of words
        :return: multi one-hot vector definite words
        """
        if len(X) == 0:
            vocabulary_size = len(self.categories_[0])
            return np.zeros((1, vocabulary_size))
        return np.any(super().transform(np.array(X).reshape(-1, 1)), axis=0).reshape(-1).astype(int)


class ToBagOfWordsVectorPatterns(CountVectorizer):
    def __init__(self, is_sparse: bool = False, **kwargs):
        self.is_sparse = is_sparse
        super().__init__(**kwargs)

    def fit(self, X: np.ndarray[np.str_], y: None = None):
        """
        :param X: array of all words
        :param y: Ignored. This parameter exists only for compatibility with :class:`~sklearn.pipeline.Pipeline`.
        :return: self
        """
        super().fit(X, y)
        return self

    def transform(self, X: list[str]) -> np.ndarray[np.int_] | csr_matrix:
        """
        :param X: array of words
        :return: bag of words in numpy array format or sklearn sparse matrix format
        """
        _X = [" ".join(X)]
        vector = super().transform(_X)
        if self.is_sparse:
            return vector
        return vector.toarray().reshape(-1)


class ToTFIDFVectorPatterns(TfidfVectorizer):
    def __init__(self, is_sparse: bool = False, **kwargs):
        self.is_sparse = is_sparse
        super().__init__(**kwargs)

    def fit(self, X: np.ndarray[np.str_], y: None = None):
        """
        :param X: array of all words
        :param y: Ignored. This parameter exists only for compatibility with :class:`~sklearn.pipeline.Pipeline`.
        :return: self
        """
        super().fit(X, y)
        return self

    def transform(self, X: list[str]) -> np.ndarray[np.float_] | csr_matrix:
        """
        :param X: array of words
        :return: tfidf representation of words in numpy array format or sklearn sparse matrix format
        """
        _X = [" ".join(X)]
        vector = super().transform(_X)
        if self.is_sparse:
            return vector
        return vector.toarray().reshape(-1)


class SparseTensorTransformer(BaseEstimator, TransformerMixin):
    @staticmethod
    def transform_func(X: csr_matrix):
        """
        Converts a sparse matrix to a sparse tensor.
        :param X: csr vector to be processed.
        :return: sparse tensor.
        """

        sparse_vector = X
        # todo: не получается создать пустой SparseTensor,
        #  придётся указать какой-нибудь один элемент единичный,
        #  модель на такой вектор ничего не предсказывает выше 16%
        sparse_tensor = tf.sparse.SparseTensor(indices=[(0, 0)], values=[1.0], dense_shape=(1, sparse_vector.shape[1]))
        if 0 < sparse_vector.nnz < sparse_vector.shape[1]:
            sparse_tensor = tf.sparse.SparseTensor(
                indices=list(zip(*sparse_vector.nonzero())), values=sparse_vector.data, dense_shape=sparse_vector.shape
            )
        return tf.sparse.reorder(sparse_tensor)

    def transform(self, X: csr_matrix):
        """
        Converts a sparse matrix to a sparse tensor.
        :param X: csr vector to be processed.
        :return: sparse tensor.
        """
        return self.transform_func(X)


class ToSentenceEmbeddingVectorPatternsSBert(BaseEstimator, TransformerMixin):
    def __init__(self, model_name: str = "sberbank-ai/sbert_large_nlu_ru", cache_dir: Path = None):
        self.model_name = model_name
        self.cache_dir = cache_dir
        if self.cache_dir is None:
            self.cache_dir = PathStorage.get_path_to_models() / model_name.split("/")[-1]
        self.model_tokenizer = AutoTokenizer.from_pretrained(model_name, cache_dir=self.cache_dir)
        self.model_feature_extractor = AutoModel.from_pretrained(model_name, cache_dir=self.cache_dir)

    def transform(self, X: list[str]) -> np.ndarray[np.float_]:
        """
        :param X: array of words.
        :return: words embedding vector.
        """
        vector_representation_of_words_torch_tensor = self.calc_vector_representation_of_words(X)
        vector_representation_of_words = np.array([x.numpy() for x in vector_representation_of_words_torch_tensor])
        vector_representation_of_words = vector_representation_of_words.reshape(1, -1)
        return vector_representation_of_words

    def calc_vector_representation_of_words(self, X: list[str]):
        """
        :param X: array of words.
        :return: words embedding vector.
        """
        try:
            # Tokenize sentences
            encoded_input = self.model_tokenizer(" ".join(X), return_tensors="pt")
            # Compute token embeddings
            with torch.no_grad():
                model_output = self.model_feature_extractor(**encoded_input)
            # Perform pooling. In this case, mean pooling
            vector_representation_of_words = self.mean_pooling(model_output, encoded_input["attention_mask"])[0]
            return vector_representation_of_words
        except RuntimeError:
            return self.transform(X[: len(X) // 2])

    @staticmethod
    def mean_pooling(
        model_output: BaseModelOutputWithPoolingAndCrossAttentions, attention_mask: torch.Tensor
    ) -> torch.Tensor:
        # First element of model_output contains all token embeddings
        token_embeddings = model_output[0]
        input_mask_expanded = attention_mask.unsqueeze(-1).expand(token_embeddings.size()).float()
        sum_embeddings = torch.sum(token_embeddings * input_mask_expanded, 1)
        sum_mask = torch.clamp(input_mask_expanded.sum(1), min=1e-9)
        return sum_embeddings / sum_mask
