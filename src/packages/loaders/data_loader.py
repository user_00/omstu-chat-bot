from __future__ import annotations
import json
import pathlib

# pylint: disable=unused-import
from os import PathLike
from typing import TypeAlias

import numpy as np
import pandas as pd
from src.packages.path_storage.path_storage import PathStorage

__all__ = ["DataLoader"]


class DataLoaderException(Exception):
    """
    Class-exception for any errors with data loading.
    """


PathType: TypeAlias = "pathlib.Path | PathLike | str"
IntentType: TypeAlias = "dict[str: str, str: str, str: str, str: list[str], str: list[str]]"


class DataLoader:
    """
    Class DataLoader responding for loading data in json format.
    """

    __DATA_FILE_PATH = PathStorage.get_path_to_data() / "intents" / "Intents.json"
    __path = pathlib.Path("")
    __encoding = "utf-8"
    __reading_mode = "rb"

    # todo: как типизировать (можно попробовать разбить на микротипы)
    @classmethod
    def __set_path(cls, path: PathType):
        """
        :param path: path to file
        :raise DataLoaderException: if unexpected error, failed to generate path
        :raise DataLoaderException: if file not found
        :raise DataLoaderException: if the path does not point to the file
        """

        try:
            cls.__path = pathlib.Path(path).resolve()
            if not cls.__path.exists():
                raise DataLoaderException(f"File not found, check your path: {path}.")
            if not cls.__path.is_file():
                raise DataLoaderException(f"Specify the path to the file, check your path: {path}.")
        except Exception as exception:
            raise DataLoaderException(f"Unexpected error, failed to generate path: {exception}") from exception

    @classmethod
    def load_json(cls, path: PathType, *args, encoding: str = "utf-8", **kwargs) -> dict:
        """
        :param path: path to file
        :param args: args which will be passed to the json load function
        :param encoding: file encoding
        :param kwargs: kwargs which will be passed to the json load function
        :return: json file reformed to dict
        :raise DataLoaderException: if unexpected error.
        """
        cls.__set_path(path)
        cls.__encoding = encoding
        try:
            with open(cls.__path, encoding=cls.__encoding) as file:
                return json.load(file, *args, **kwargs)
        except Exception as exception:
            raise DataLoaderException(f"Unexpected error: {exception}") from exception

    @classmethod
    def load_numpy_array(cls, path: PathType, *args, **kwargs) -> np.ndarray[any]:
        """
        :param path: path to file
        :param args: args which will be passed to the numpy load function
        :param kwargs: kwargs which will be passed to the numpy load function
        :return: numpy array
        :raise DataLoaderException: if unexpected error.
        """
        cls.__set_path(path)
        try:
            return np.load(str(path), allow_pickle=True, *args, **kwargs)
        except Exception as exception:
            raise DataLoaderException(f"Unexpected error: {exception}") from exception

    @classmethod
    def load_pandas_dataframe(cls, path: PathType, *args, read_func: callable = pd.read_csv, **kwargs) -> pd.DataFrame:
        """
        :param path: path to file
        :param args: args which will be passed to the pandas load function
        :param read_func: function to load data from pandas library
        :param kwargs: kwargs which will be passed to the pandas load function
        :return: pandas dataframe
        :raise DataLoaderException: if unexpected error.
        """
        cls.__set_path(path)
        if "encoding" in kwargs:
            cls.__encoding = kwargs["encoding"]
        try:
            with open(cls.__path, encoding=cls.__encoding) as file:
                return read_func(file, *args, **kwargs)
        except Exception as exception:
            raise DataLoaderException(f"Unexpected error: {exception}") from exception

    @classmethod
    def load_data(
        cls,
    ) -> dict[str:str, str:str, str : list[IntentType]]:
        """
        :return: data json file.
        :raise DataLoaderException: if data file doesn't exists.
        """
        return cls.load_json(cls.__DATA_FILE_PATH)

    @classmethod
    def load_intents(cls) -> IntentType:
        """
        :return: data json file.
        :raise DataLoaderException: if data file doesn't exists.
        """
        # todo:
        return cls.load_data().get("intents")
