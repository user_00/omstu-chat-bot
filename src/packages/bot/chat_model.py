"""
This module operation all bot operations: loading models, get intent, answer and so on.
"""
from __future__ import annotations
from typing import TypeAlias
import numpy as np
from src.packages.loaders import DataLoader
from src.packages.loaders import PipelineLoader
from src.packages.loaders import config
from src.packages.path_storage.path_storage import PathStorage

PredictedIntents: TypeAlias = np.ndarray[dict[str, float]]
# VectorizedText: TypeAlias = np.ndarray[np.int_ | np.float]


class ChatModelException(Exception):
    """
    Class-exception for any errors with ChatModel.
    """


class ChatModel:
    """
    Class ChatModel responding for all bots operations.
    """

    def __init__(self, model_name: str = None, threshold: float = None) -> None:
        """
        Class initialization with loading models.
        :param model_name: model name, the list of available models can be seen in the condition below.
        :param threshold: probability threshold for triggering an intent.
        """
        self.threshold = threshold
        self.model_name = model_name
        if self.model_name is None:
            self.model_name = config["model_name"]
        if self.threshold is None:
            self.threshold = config["threshold_model"]
        self.model_name = self.model_name.lower()
        if self.model_name == "perceptron":
            self.__model = PipelineLoader.load_pipeline("perceptron_full_pipeline")
        elif self.model_name == "perceptron_dense":
            self.__model = PipelineLoader.load_pipeline("perceptron_dense_full_pipeline")
        elif self.model_name == "perceptron_sparse":
            self.__model = PipelineLoader.load_pipeline("perceptron_sparse_full_pipeline")
        elif self.model_name == "rnn":
            raise ChatModelException(f"Model {self.model_name} not yet available.")
        elif self.model_name == "wmd":
            raise ChatModelException(f"Model {self.model_name} not yet available.")
        else:
            raise ChatModelException(f"Invalid parameter: {self.model_name}.")
        self._intents_path = PathStorage.get_path_to_data() / "unique_sorted_intents.npy"
        self.__load_intents()

    def __load_intents(self, intents: np.ndarray[np.str_] = None):
        """
        Loading intents, if it does not exists.
        :param intents: custom class responsible for loading intents.
        """
        if intents is None:
            self.__intents = DataLoader.load_numpy_array(self._intents_path)
        else:
            self.__intents = intents

    def get_intents(self, text: str) -> PredictedIntents:
        """
        Returns the intents object for the input text.
        :param text: some question from user.
        :return: intent for user's question.
        """
        predicted_intents = self.__make_prediction(text)
        return predicted_intents

    def get_intent(self, text: str) -> str:
        """
        Returns the most likely intent for the input text.
        :param text: some question from user.
        :return: intent for user's question.
        """
        predicted_intents = self.get_intents(text)
        most_likely_intention = predicted_intents[0]
        return most_likely_intention.get("intent")

    def __make_prediction(self, text: str) -> PredictedIntents:
        """
        Making prediction on vectorized text.
        :param text: some question from user.
        :return: prediction on the question.
        :raise ChatModelException: if the perceptron exception occurs.
        """
        try:
            intention_probability_vector = self.__model.predict(text)
            array_of_predicted_intentions = [
                (intent_number, probability)
                for intent_number, probability in enumerate(intention_probability_vector)
                if probability > self.threshold
            ]
            if len(array_of_predicted_intentions) == 0:
                return np.array([{"intent": "NONE", "probability": 1.0}])
            array_of_predicted_intentions.sort(key=lambda x: x[1], reverse=True)
            array_of_dict_predicted_intentions = []
            for intent_number, probability in array_of_predicted_intentions:
                array_of_dict_predicted_intentions.append(
                    {"intent": self.__intents[intent_number], "probability": probability}
                )
            return np.array(array_of_dict_predicted_intentions)
        except Exception as exception:
            raise ChatModelException(f"Model predict exception: {exception}.") from exception
