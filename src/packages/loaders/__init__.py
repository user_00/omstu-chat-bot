"""
Initialization file for the package __loaders__.
"""
from .config_loader import *
from .model_loader import *
from .pipeline_loader import *
from .data_loader import *

config = ConfigLoader.load_config()
env_variables = ConfigLoader.load_env_variables()

__all__ = ["config", "env_variables", "ModelLoader", "DataLoader", "PipelineLoader"]
