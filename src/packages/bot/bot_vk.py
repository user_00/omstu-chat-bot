# pylint: skip-file
from vk_api import VkApi
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.utils import get_random_id
from .IBot import IBot
import time
import requests
from src.packages.logger import logger, Loggers

__all__ = ["BotVk"]


class BotVk(IBot):
    _IBot__chat_bot = None
    _IBot__logger = None
    __vk_api_access = None
    __long_pool = None

    def start(self):
        logger.info(Loggers.APP.value, "Starting VK bot...")
        self.connect_and_listen()
    def connect_and_listen(self):
        try:
            for event in self.__long_pool.listen():
                if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text and event.from_user:
                    message_text = event.text
                    receiver_user_id = event.user_id
                    if message_text in ("/start", "!start"):
                        self._IBot__handle_start(receiver_user_id)
                    elif message_text in ("/help", "!help"):
                        self._IBot__handle_help(receiver_user_id)
                    else:
                        self._IBot__handle_text(message_text, receiver_user_id)
        except requests.exceptions.ReadTimeout:
            logger.error("\n ReadTimeout exception. Reconnecting to VK servers \n")
            time.sleep(20)
            self.connect_and_listen()
        except Exception as e:  # Catch any other exceptions
            logger.error(f"\n Unexpected error: {e}. Reconnecting to VK servers \n")
            time.sleep(20)
            self.connect_and_listen()


    def _IBot__authorization(self, api_key: str):
        vk_session = VkApi(token=api_key)
        self.__vk_api_access = vk_session.get_api()
        self.__long_pool = VkLongPoll(vk_session)

    def _IBot__handle_start(self, receiver_user_id: int):
        """
        This handler will be called when user sends `/start` command.
        """
        self.__vk_api_access.messages.send(
            user_id=receiver_user_id, message=self._IBot__get_start_message(), random_id=get_random_id()
        )


    def _IBot__handle_help(self, receiver_user_id: int):
        """
        This handler will be called when user sends `/help` command.
        """
        self.__vk_api_access.messages.send(
            user_id=receiver_user_id, message=self._IBot__get_help_message(), random_id=get_random_id()
        )

    def _IBot__handle_text(self, message_text: str, receiver_user_id: int):
        """
        This handler will be called when the user sends any text.
        Based on the received question, and answer is generated and sent to the user.
        """
        try:
            self._IBot__chat_bot.set_user_id(receiver_user_id)
            answer = self._IBot__chat_bot.generate_answer(message_text)
            self.__vk_api_access.messages.send(user_id=receiver_user_id, message=answer, random_id=get_random_id())
        except RuntimeError as error:
            self._IBot__logger.error("app", str(error))
