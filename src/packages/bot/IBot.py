# pylint: skip-file
from abc import ABC, abstractmethod
from src.packages.loaders import config
from .chat_bot import ChatBot


class BotException:
    pass


class IBot(ABC):
    __chat_bot = None

    def __init__(self, api_key: str, platform, chat_model) -> None:
        """
        Initialize IBot class with `logger` and authorize using `api_key`.
        :param api_key: api key для получения доступу для получения и отправки сообщений.
        """
        self.__chat_bot = ChatBot(platform, chat_model)
        self.__authorization(api_key)

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def __authorization(self, api_key: str) -> None:
        pass

    @abstractmethod
    def __handle_start(self):
        pass

    @abstractmethod
    def __handle_help(self):
        pass

    @abstractmethod
    def __handle_text(self):
        pass

    @staticmethod
    def __get_start_message():
        return config["onboarding"]

    @staticmethod
    def __get_help_message():
        return config["connect_with_creators"]
