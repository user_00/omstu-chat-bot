# pylint: skip-file
"""
This file is the main one, the bot is launched from it.
"""
from src.packages.bot import BotTelegram
from src.packages.bot import BotVk
from src.packages.bot import BotDS
from src.packages.logger import logger, Loggers
from src.packages.loaders import env_variables
import asyncio
import threading
from src.packages.bot.chat_model import ChatModel

logger.info(Loggers.APP.value, "Chatbot session started;")

loop = asyncio.get_event_loop()
if loop.is_closed():
    loop = asyncio.new_event_loop()

if __name__ == "__main__":
    try:
        threads = []

        # Load the model once
        logger.info(Loggers.APP.value, "Load Model")
        chat_model = ChatModel()

        if env_variables["API_KEY_TELEGRAM"]:
            bot_telegram = BotTelegram(env_variables["API_KEY_TELEGRAM"], "tg", chat_model)
            thread = threading.Thread(target=bot_telegram.start)
            threads.append(thread)

        if env_variables["API_KEY_VK"]:
            bot_vk = BotVk(env_variables["API_KEY_VK"], "vk", chat_model)
            thread = threading.Thread(target=bot_vk.start)
            threads.append(thread)

        if env_variables["API_KEY_DS"]:
            bot_ds = BotDS(env_variables["API_KEY_DS"], "ds", chat_model)
            thread = threading.Thread(target=bot_ds.start)
            threads.append(thread)

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()
    except KeyboardInterrupt:
        logger.info(Loggers.APP.value, "Chat-bot session interrupted.")
    logger.info(Loggers.APP.value, "Chatbot session ended.")
