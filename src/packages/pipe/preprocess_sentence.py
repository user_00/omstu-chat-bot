import re
from json import JSONDecodeError
from ssl import SSLError

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_is_fitted
from nltk.corpus import stopwords
from pyxdameraulevenshtein import normalized_damerau_levenshtein_distance_seqs
import pyaspeller
import pymorphy2
import nltk
import numpy as np
from src.packages.logger import logger, Loggers

__all__ = [
    "RemoveGarbage",
    "RemovePunctuation",
    "Tokenize",
    "CorrectOrthography",
    "FixTypos",
    "RemoveStopWords",
    "ToBaseForm",
]


class RemoveGarbage(BaseEstimator, TransformerMixin):
    @staticmethod
    def transform_func(X: str) -> str:
        """
        Removing garbage from text.
        :param X: raw text to be processed.
        :return: processed text containing only letters, spaces and  punctuation symbols.
        """
        _X = X.replace("\u00A0", " ")
        _X = _X.replace(" ", " ")
        return re.sub("[^А-ЯЁёа-яA-Za-z-,.!?:;…–—() ]", "", _X)

    def transform(self, X: str) -> str:
        """
        Removing garbage from text.
        :param X: raw text to be processed.
        :return: processed text containing only letters, spaces and  punctuation symbols.
        """
        return self.transform_func(X)


class RemovePunctuation(BaseEstimator, TransformerMixin):
    @staticmethod
    def transform_func(X: str) -> str:
        """
        Removing punctuation symbols from text.
        :param X: raw text to be processed.
        :return: processed text containing only letters and spaces.
        """
        return re.sub("[^А-ЯЁёа-яA-Za-z- ]", "", X)

    def transform(self, X: str) -> str:
        """
        Removing punctuation symbols from text.
        :param X: raw text to be processed.
        :return: processed text containing only letters and spaces.
        """
        return self.transform_func(X)


class Tokenize(BaseEstimator, TransformerMixin):
    @staticmethod
    def transform_func(X: str) -> list:
        """
        Tokenizing text.
        :param X: raw text to be processed.
        :return: list with words.
        """
        raw_text = X.lower()
        tokens = nltk.word_tokenize(raw_text)
        return tokens

    def transform(self, X: str) -> list:
        """
        Tokenizing text.
        :param X: raw text to be processed.
        :return: list with words.
        """
        return self.transform_func(X)


class CorrectOrthography(BaseEstimator, TransformerMixin):
    def __init__(self, lang=None, ignore_uppercase=None, **kwargs):
        if lang is None:
            lang = ["en", "ru"]
        self.lang = lang
        # The library has not been updated, at the moment the API does not support
        # this ignore_uppercase parameter. The implementation of this parameter is done on the client side below.
        self.ignore_uppercase = ignore_uppercase
        self.speller = pyaspeller.YandexSpeller(lang=self.lang, ignore_uppercase=self.ignore_uppercase, **kwargs)

    def transform(self, X: str) -> str:
        """
        Checks and corrects spelling errors and typos in sentences.
        :param X: sentence to check. The YandexSpeller API has restrictions on the length of
        the text being checked, so it is not recommended to submit text longer than 40k characters.
        :return: corrected sentence.
        """
        try:
            sentence = X
            for found_spelling_mistake in self.speller.spell(X):
                if found_spelling_mistake["s"]:
                    if self.ignore_uppercase and found_spelling_mistake["word"].isupper():
                        continue
                    original_word = found_spelling_mistake["word"]
                    most_likely_correct_word = found_spelling_mistake["s"][0]
                    sentence = sentence.replace(original_word, most_likely_correct_word)
            return sentence
        except JSONDecodeError as exception:
            if exception.doc:
                if exception.doc == "Request entity too large":
                    logger.warning(Loggers.APP.value, f"JSONDecode Error: {exception.doc}")
                    return X
                if exception.doc == "Service unavailable":
                    logger.info(Loggers.APP.value, f"JSONDecode Error: {exception.doc}")
                    return X
            logger.warning(Loggers.APP.value, f"JSONDecode Error: {exception}, {exception.doc}")
            return X
        except SSLError as exception:
            logger.warning(Loggers.APP.value, f"SSL Error: {exception}")
            return X
        except Exception as exception:
            logger.error(Loggers.APP.value, f"Unexpected error: {exception}")
            return X


class FixTypos(BaseEstimator, TransformerMixin):
    def __init__(self, threshold=0.45):
        self._is_fitted = None
        self.vocabulary = None
        self.threshold = threshold

    def __sklearn_is_fitted__(self):
        return hasattr(self, "_is_fitted") and self._is_fitted

    def fit(self, X: np.ndarray[np.str_], y: None = None):
        """
        :param X: array of all words
        :param y: Ignored. This parameter exists only for compatibility with :class:`~sklearn.pipeline.Pipeline`.
        :return: self
        """
        self.vocabulary = X
        self._is_fitted = True
        return self

    def transform(self, X: str) -> str:
        """
        Checks and corrects word errors.
        :param X: word to check.
        :return: if word contains errors > 45% - return uncorrected word, else - corrected word.
        """
        check_is_fitted(self)
        # todo: почитать про normalized_damerau_levenshtein_distance_seqs
        distance_vector = normalized_damerau_levenshtein_distance_seqs(X, self.vocabulary)
        word_index_with_min_distance = np.argmin(distance_vector)
        min_distance = distance_vector[word_index_with_min_distance]
        if min_distance > self.threshold:
            return X
        return self.vocabulary[word_index_with_min_distance]


class RemoveStopWords(BaseEstimator, TransformerMixin):
    is_punkt_downloaded = nltk.download("punkt", quiet=True)
    is_stopwords_downloaded = nltk.download("stopwords", quiet=True)

    stop_words = set(stopwords.words(["russian", "english"]))

    def __check_if_dictionaries_are_installed(self):
        if self.is_stopwords_downloaded and self.is_punkt_downloaded:
            return True
        logger.warning(Loggers.APP.value, "Failed to install nltk dictionaries")
        return False

    def transform(self, X: list[str]) -> list[str]:
        """
        Removing stop words from tokenized text.
        :param X: list, that contains tokens.
        :return: list, without stop words.
        """
        self.__check_if_dictionaries_are_installed()
        filtered_tokens = [word for word in X if word not in self.stop_words]
        return filtered_tokens


class ToBaseForm(BaseEstimator, TransformerMixin):
    @staticmethod
    def transform_func(X: list[str]) -> list[str]:
        """
        Brings the words back to its base form.
        :param X: raw text, which needs to be processed.
        :return: list in which words are reduced to their base form.
        """
        try:
            morph = pymorphy2.MorphAnalyzer()
            base_form = []
            for word in X:
                if len(word) < 2:
                    continue
                word = morph.parse(word)[0]
                base_form.append(word.normal_form)
            return base_form
            # todo: Может быть исключение https (бот сказал, что не может быть)?
        except Exception as exception:
            logger.warning(Loggers.APP.value, f"Unexpected error: {exception}")
            return X

    def transform(self, X: list[str]) -> list[str]:
        """
        Brings the words back to its base form.
        :param X: raw text, which needs to be processed.
        :return: list in which words are reduced to their base form.
        """
        return self.transform_func(X)
