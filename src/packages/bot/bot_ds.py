# pylint: skip-file
from .IBot import IBot
import discord
from discord.ext import commands
from src.packages.logger import Log
from src.packages.logger import logger, Loggers


class BotDS(IBot):
    _IBot__chat_bot = None
    _IBot__logger = None

    def __init__(self, api_key: str, platform="ds", chat_model=None):
        super().__init__(api_key, platform, chat_model)
        intent = discord.Intents.default()
        intent.members = True
        intent.message_content = True  # для windows законментировать строчку
        self.client = discord.Client(intents=intent)
        self.client.event(self.on_message)
        self.api_key = api_key

    def start(self):
        logger.info(Loggers.APP.value, "Starting DS bot...")
        self.client.run(self.api_key)


    def _IBot__authorization(self, api_key: str):
        self._IBot__api_key = api_key

    async def _IBot__handle_start(self, message):
        """
        This handler will be called when user sends `/start` command.
        """
        await message.reply(self._IBot__get_start_message())

    async def _IBot__handle_help(self, message):
        """
        This handler will be called when user sends `/help` command.
        """
        await message.reply(self._IBot__get_help_message())

    async def _IBot__handle_text(self, message):
        """
        This handler will be called when the user sends any text.
        Based on the received question, and answer is generated and sent to the user.
        """
        try:
            self._IBot__chat_bot.set_user_id(message.author.id)
            answer = self._IBot__chat_bot.generate_answer(message.content)
            await message.reply(answer)
            # await message.channel.send(answer)
        except RuntimeError as error:
            self._IBot__logger.error("app", str(error))

    async def on_message(self, message):
        if message.author != self.client.user:
            if message.content in ("/start", "!start"):
                await self._IBot__handle_start(message)
            elif message.content in ("/help", "!help"):
                await self._IBot__handle_help(message)
            else:
                await self._IBot__handle_text(message)
