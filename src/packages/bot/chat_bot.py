# pylint: skip-file

"""
This module contains ChatBot functions associated with answers.
"""
import random

from src.packages.logger import Loggers, logger
from src.packages.bot.chat_model import ChatModel, PredictedIntents
from src.packages.loaders import config


class ChatBotException(Exception):
    """
    Class-exception for any errors with bot working.
    """


def _get_random_answer_by_intent(intent: str) -> str:
    """
    Getting random answer for specific intent.
    :param intent: intent for user question.
    :return: random selected response for question.
    """
    responses = config["intents"][intent]["responses"]
    return random.choice(responses)


def _get_reserve_answer() -> str:
    """
    Getting reserve answer, return failure phrase from `bot_config`.
    :return: string with reserve answer.
    """
    answer = random.choice(config["failure_phrases"])
    return answer


def _get_product_intent_names(intents: PredictedIntents) -> list:
    """
    Getting reserve answer, return failure phrase from `bot_config`.
    :return: string with reserve answer.
    """
    intent_name_list = []
    for intent in intents:
        intent_ = config.get("intents")[intent["intent"]]
        if intent_["type"] == "product":
            intent_name_list.append(intent_["name"])
    return intent_name_list


def _get_predictive_clarifying_answer(intent_names: list) -> str:
    """
    Getting reserve answer, return failure phrase from `bot_config`.
    :return: string with reserve answer.
    """
    answer = random.choice(config["predictive_clarifying"])
    answer = answer + "\n".join(intent_names)
    return answer


def _log_question(text: str, intent: str, platform, user_id) -> None:
    """
    Logging user question.
    :param text: some question from user.
    :param intent: intent for this question.
    """
    logger.info(Loggers.INCOMING.value, f'"{text}" - {str(intent)} - {platform} - {user_id};')


class ChatBot:
    """
    A chat-bot class with the functionality necessary to generate a response to a user's message.
    """

    __chat_model = None
    __logger = None
    __platform = "test"
    __user_id = None

    def __init__(self, platform, chat_model) -> None:
        """
        Initialize ChatBot class with logger.
        """
        self.__chat_model = chat_model
        self.__platform = platform

    def set_user_id(self, user_id):
        """
        Set user id for the current session.
        :param user_id: The id of the user
        """
        self.__user_id = user_id

    def generate_answer(self, text: str) -> str:
        """
        Initialize ChatBot class with logger.
        :param text: some question from user.
        :return: if intent available - return random answer from bot config for specific intent,
        else - reserve answer.
        """
        if len(text) <= int(config["one_character_scenario_num_characters"]):
            return random.choice(config["one_character_scenario"])
        intents = self.__chat_model.get_intents(text)
        intent = intents[0]
        _log_question(text, intent, self.__platform, self.__user_id)
        if intent["intent"] == "NONE" and len(intents) == 1:
            return _get_reserve_answer()
        intent_names = _get_product_intent_names(intents)
        if intent["probability"] <= config["threshold_predictive_clarifying"] and len(intent_names) > 0:
            return _get_predictive_clarifying_answer(intent_names)
        return _get_random_answer_by_intent(intent["intent"])
