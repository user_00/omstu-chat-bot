import json

# Загрузка JSON
with open("/content/Intents.json", "r", encoding="utf-8") as f:
    data = json.load(f)

# Обработка каждого намерения
for intent in data["intents"]:
    # Удаление дубликатов из 'patterns' с сохранением порядка
    unique_patterns = list(dict.fromkeys(intent["patterns"]))
    intent["patterns"] = unique_patterns

# Сохранение измененного JSON
with open("/content/Intents.json", "w", encoding="utf-8") as f:
    json.dump(data, f, ensure_ascii=False, indent=4)
