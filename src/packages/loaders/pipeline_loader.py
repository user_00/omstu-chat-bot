from __future__ import annotations
from sklearn.pipeline import make_pipeline, Pipeline, FeatureUnion
from src.packages.loaders import ModelLoader
from src.packages.path_storage import PathStorage
from src.packages.pipe.perceptron.preprocess_data import ToSentenceEmbeddingVectorPatternsSBert, SparseTensorTransformer
from src.packages.pipe.common import ModelPrediction, ReshapeTransformer, NormalizationStep

__all__ = ["PipelineLoader"]


class PipelineLoaderException(Exception):
    """
    Exception class for the pipeline loader.
    """


class PipelineLoader:
    @classmethod
    def load_pipeline(cls, pipeline_name: str = None) -> any:
        """
        The method loads the pipeline by name.
        :param pipeline_name: name of the pipeline, available names are listed below.
        :raise PipelineLoaderException: if invalid parameter with pipeline name.
        """

        try:
            pipeline_name = pipeline_name.lower()
            pipeline_name = pipeline_name.replace(" ", "_")
            if pipeline_name in {"preprocess_sentence", "fix_typos"}:
                return ModelLoader.load(PathStorage.get_path_to_models() / "pipelines" / f"{pipeline_name}.joblib")
            if pipeline_name == "primary_pretreatment_pipe":
                return make_pipeline(cls.load_pipeline("preprocess_sentence"), cls.load_pipeline("fix_typos"))
            if pipeline_name == "to_predict_dense":
                encoder_for_patterns_dense = ToSentenceEmbeddingVectorPatternsSBert()
                return make_pipeline(encoder_for_patterns_dense)
            if pipeline_name == "to_predict_sparse":
                encoder_for_patterns_sparse = ModelLoader.load(
                    PathStorage.get_path_to_models() / "pipelines" / "perceptron" / "tfidf_encoder_for_patterns.joblib"
                )
                return make_pipeline(encoder_for_patterns_sparse, SparseTensorTransformer())
            if pipeline_name == "make_predict_dense":
                model_dense = ModelLoader.load(
                    PathStorage.get_path_to_models() / "sbert_and_perceptron" / "perceptron.h5"
                )
                return make_pipeline(ModelPrediction(model_dense), ReshapeTransformer((-1, 1)))
            if pipeline_name == "make_predict_sparse":
                model_sparse = ModelLoader.load(PathStorage.get_path_to_models() / "perceptron" / "perceptron.h5")
                return make_pipeline(ModelPrediction(model_sparse), ReshapeTransformer((-1, 1)))
            if pipeline_name == "to_predict_and_make_predict_dense":
                return make_pipeline(cls.load_pipeline("to_predict_dense"), cls.load_pipeline("make_predict_dense"))
            if pipeline_name == "to_predict_and_make_predict_sparse":
                return make_pipeline(cls.load_pipeline("to_predict_sparse"), cls.load_pipeline("make_predict_sparse"))
            if pipeline_name == "perceptron_dense_full_pipeline":
                return make_pipeline(
                    cls.load_pipeline("primary_pretreatment_pipe"),
                    cls.load_pipeline("to_predict_dense"),
                    cls.load_pipeline("make_predict_dense"),
                )
            if pipeline_name == "perceptron_sparse_full_pipeline":
                return make_pipeline(
                    cls.load_pipeline("primary_pretreatment_pipe"),
                    cls.load_pipeline("to_predict_sparse"),
                    cls.load_pipeline("make_predict_sparse"),
                )
            if pipeline_name == "perceptron_full_pipeline":
                return Pipeline(
                    [
                        ("primary_pretreatment_pipe", cls.load_pipeline("primary_pretreatment_pipe")),
                        (
                            "make_predict",
                            FeatureUnion(
                                [
                                    (
                                        "to_predict_and_make_predict_dense",
                                        cls.load_pipeline("to_predict_and_make_predict_dense"),
                                    ),
                                    (
                                        "to_predict_and_make_predict_sparse",
                                        cls.load_pipeline("to_predict_and_make_predict_sparse"),
                                    ),
                                ]
                            ),
                        ),
                        ("normalization", NormalizationStep()),
                    ]
                )
            raise PipelineLoaderException(f"Invalid parameter: {pipeline_name}.")
        except Exception as exception:
            raise PipelineLoaderException(f"Unexpected error: {exception}") from exception
