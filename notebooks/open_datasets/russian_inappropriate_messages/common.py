from src.packages.path_storage import PathStorage

path_to_data = PathStorage.get_path_to_notebooks() / "open_datasets" / "russian_inappropriate_messages" / "data"
path_to_models = PathStorage.get_path_to_notebooks() / "open_datasets" / "russian_inappropriate_messages" / "models"
